package primitivas;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Event;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.util.HashSet;
import javax.swing.JPanel;

public class Primitivas extends javax.swing.JFrame {

    Point punto1, punto2;
    boolean dibujar;

    Rastreo rastreo;

    int ancho = 640;
    int alto = 480;

    boolean firstTime = true;

    public Primitivas() {
        punto1 = new Point();
        punto2 = new Point();
        dibujar = false;
        rastreo = new Rastreo(640, 480);
        this.setSize(ancho + 10, alto + 10);

        this.setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        this.setLayout(new BorderLayout());

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent evt) {
                jPanel1MouseClicked(evt);
            }
        });
        this.setBackground(Color.white);
        this.setVisible(true);

    }

    @Override
    public void update(Graphics g) {
        paint(g);
    }

    public void paint(Graphics g) {
        if (firstTime) {
            firstTime = false;
            super.paint(g);
        }

        Image output = rastreo.toImage(this);
        g.drawImage(output, 0, 0, this);
    }

    public void clear() {
        int s = rastreo.size();
        for (int i = 0; i < s; i++) {
            rastreo.pixel[i] ^= 0x00ffffff;
        }
        repaint();
        return;
    }

    public void linea(int x0, int y0, int x1, int y1, Color color) {
        int pixel = color.getRGB();
        int dx = x1 - x0;
        int dy = y1 - y0;

        rastreo.setPixel(pixel, x0, y0);

        if (dx != 0) {
            float m = (float) dy / (float) dx;
            float b = y0 - m * x0;

            dx = (x1 > x0) ? 1 : -1;

            while (x0 != x1) {
                x0 += dx;
                y0 = Math.round(m * x0 + b);
                rastreo.setPixel(pixel, x0, y0);

            }
        }
    }

    public void lineaNueva(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dx = x1 - x0;
        int dy = y1 - y0;
        rastreo.setPixel(pix, x0, y0);
        if (Math.abs(dx) > Math.abs(dy)) {     // inclinacion < 1
            float m = (float) dy / (float) dx; // calcular inclinacion
            float b = y0 - m * x0;
            dx = (dx < 0) ? -1 : 1;
            while (x0 != x1) {
                x0 += dx;
                rastreo.setPixel(pix, x0, Math.round(m * x0 + b));
            }
        } else if (dy != 0) {                         // inclinacion >= 1
            float m = (float) dx / (float) dy; // Calcular inclinacion
            float b = x0 - m * y0;
            dy = (dy < 0) ? -1 : 1;
            while (y0 != y1) {
                y0 += dy;
                rastreo.setPixel(pix, Math.round(m * y0 + b), y0);
            }
        }
    }

    public void lineFast(int x0, int y0, int x1, int y1, Color color) {
        int pix = color.getRGB();
        int dy = y1 - y0;
        int dx = x1 - x0;
        int stepx, stepy;
        if (dy < 0) {
            dy = -dy;
            stepy = -rastreo.width;
        } else {
            stepy = rastreo.width;
        }
        if (dx < 0) {
            dx = -dx;
            stepx = -1;
        } else {
            stepx = 1;
        }
        dy <<= 1;
        dx <<= 1;
        y0 *= rastreo.width;
        y1 *= rastreo.width;
        rastreo.pixel[x0 + y0] = pix;
        if (dx > dy) {
            int fraction = dy - (dx >> 1);
            while (x0 != x1) {
                if (fraction >= 0) {
                    y0 += stepy;
                    fraction -= dx;
                }
                x0 += stepx;
                fraction += dy;
                rastreo.pixel[x0 + y0] = pix;
            }
        } else {
            int fraction = dx - (dy >> 1);
            while (y0 != y1) {
                if (fraction >= 0) {
                    x0 += stepx;
                    fraction -= dy;
                }
                y0 += stepy;
                fraction += dx;
                rastreo.pixel[x0 + y0] = pix;
            }
        }
    }

    private void dibujarLinea(Point _p1, Point _p2, Color color) {
        long inicio = 0, fin = 0;
        inicio = System.nanoTime();
        linea(_p1.x, _p1.y, _p2.x, _p2.y, color);
        fin = System.nanoTime();

        System.out.printf("Tiempo transcurrido, simple: %d\n", (fin - inicio));
    }

    private void jPanel1MouseClicked(java.awt.event.MouseEvent evt) {
        // TODO add your handling code here:
        if (!dibujar) {
            punto1.x = evt.getX();
            punto1.y = evt.getY();
            dibujar = true;
        } else {
            punto2.x = evt.getX();
            punto2.y = evt.getY();
            Point p3=centroCirculo(punto1,punto2);
            dibujarCirculo(p3.x, p3.y,(int) Math.round(calcularRadio(punto1,punto2)), Color.red);
            dibujarElipse(p3.x,p3.y,punto2.x,punto2.y,Color.red);
            dibujarLinea(punto1, punto2, Color.red);
            dibujar = false;
            repaint();
        }
    }

    public double calcularRadio(Point p1, Point p2){
        double radio;
        radio = (Math.sqrt(((p2.x-p1.x)*(p2.x-p1.x))+((p2.y-p1.y)*(p2.y-p1.y)))/2);
        return radio;
    }
    public Point centroCirculo(Point p1, Point p2) {
        Point p = new Point();
        p.x = (int) Math.round((p1.x + p2.x) / 2);
        p.y = (int) Math.round((p1.y + p2.y) / 2);
        return p;
    }
    //DIBUJA LA ELIPSE CON EL RASTER  
    public void dibujarElipse(int ox,int oy, int x1, int y1,Color color){
        double a, b, angulo, x3, y3, dis, teta;
        int xr, yr,altura=25;
        int pix = color.getRGB();
        a = Math.sqrt(Math.pow(x1 - ox, 2) + Math.pow(y1 - oy, 2));
    //    b = Math.sqrt(Math.pow(x2 - ox, 2) + Math.pow(y2 - oy, 2));
        xr = ox + (int) (a * Math.cos(0));
        yr = oy;
        dis = Math.sqrt(Math.pow(x1 - xr, 2));
        teta = Math.asin((dis / 2) / a);
        teta = 2 * teta;
                for (angulo = 0; angulo <= 360; angulo++) {
                        x3= ox + a*Math.cos(angulo+teta);
                        y3= oy + altura*Math.sin(angulo);
                       rastreo.setPixel(pix,(int)Math.round(x3),(int)Math.round(y3));
                }
    
    }
    // DIBUJAR EL CIRCULO CON EL RASTER
    public void dibujarCirculo(int xc, int yc, int r, Color color) {
        int pix = color.getRGB();
        double x1,y1;
        double angulo=0;
        do{
        x1=xc + r * Math.cos(angulo);
        y1=yc + r * Math.sin(angulo);
        angulo+=0.005;
        rastreo.setPixel(pix,(int)x1,(int)y1);
        }while(angulo < 6.28);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Pizarra.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                Primitivas p = new Primitivas();
                //Pizarra pizarra = new Pizarra(p);
                p.setVisible(true);
            }
        });
    }

}
